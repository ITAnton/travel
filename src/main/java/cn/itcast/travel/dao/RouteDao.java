package cn.itcast.travel.dao;

import cn.itcast.travel.domain.Favorite;
import cn.itcast.travel.domain.Route;

import java.util.List;

/**
 * @author  Created by 丶Anton on 2021/3/12 13:23
 */
public interface RouteDao {
    /**
     * 查询总记录条数
     * @param cid
     * @param rName
     * @return
     */

    int countAll(int cid, String rName);

    /**
     * 查询指定金额之间  与 模糊路线名
     * @param currentPrice
     * @param endPrice
     * @param rName
     * @return
     */
    int countAll(int currentPrice, int endPrice, String rName);

    /**
     * 1
     * @param cid
     * @param startPage
     * @param pageSize
     * @param rName
     * @return
     */
    List<Route> getLimitPageBean(int cid, int startPage, int pageSize, String rName);

    /**
     * 查询指定商品信息
     * @param rid
     * @return
     */
    Route find(int rid);

    /**
     * 查询收藏最多的5条路线
     * @return
     */
    List<Route> findRecommend();


    /**
     * 通过rid查询所有的相关路线
     * @param favorites
     * @param currentPage
     * @return
     */
    List<Route> findAllKeep(List<Favorite> favorites, int currentPage);

    /**
     * 收藏排行查询
     * @param currentPrice
     * @param endPrice
     * @param rName
     * @param current
     * @return
     */
    List<Route> findFavoritesRanking(int currentPrice, int endPrice, int current, String rName);

    /**
     * 查询主页路线
     * @param cid
     * @param field
     * @param pageSize
     * @return
     */
    List<Route> findPopularity(int cid, String field, int pageSize);

    /**
     * 查询主题路线
     * @param rName
     * @return
     */
    List<Route> findTheme(String rName);

}
