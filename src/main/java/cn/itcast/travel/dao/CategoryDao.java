package cn.itcast.travel.dao;

import cn.itcast.travel.domain.Category;

import java.util.List;

/**
 * Created by 丶Anton on 2021/3/11 17:03
 */
public interface CategoryDao {

    /**
     * 查询所有分页标签
     * @return
     */
    List<Category> findAllCategoryDao();

    /**
     * 通过cid查询指定商品路线分类名称
     * @param cid
     * @return
     */
    Category findOne(int cid);

}
