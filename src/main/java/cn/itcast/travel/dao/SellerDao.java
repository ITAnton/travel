package cn.itcast.travel.dao;

import cn.itcast.travel.domain.Seller;

/**
 * @author Created by 丶Anton on 2021/3/13 12:42
 * 商家dao
 */
public interface SellerDao {

    /**
     * 通过id查询该商品发布商家
     * @param sid
     * @return
     */
    Seller findAll(int sid);

}
