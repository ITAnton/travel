package cn.itcast.travel.dao;

import cn.itcast.travel.domain.RouteImg;

import java.util.List;

/**
 * @author Created by 丶Anton on 2021/3/13 12:40
 * 商品详情对应图片dao
 */
public interface RouteImgDao {

    /**
     * 通过rid查询该商品所有大小图片
     * @param rid
     * @return
     */
    List<RouteImg> findImgId(int rid);

}
