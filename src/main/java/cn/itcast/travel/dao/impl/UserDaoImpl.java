package cn.itcast.travel.dao.impl;

import cn.itcast.travel.dao.UserDao;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by 丶Anton on 2021/3/10 10:08
 */
public class UserDaoImpl implements UserDao {
    private JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());

    @Override
    public String findUserName(String userName) {
        String sql = "SELECT username FROM tab_user WHERE username=?";
        try {
            User user = jt.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), userName);
            return user.getUsername();
        } catch (Exception e) {
//            查询不到结果返回null
            return null;
        }
    }

    @Override
    public boolean registered(User user) {
        String sql = "INSERT INTO tab_user(username,password,name,birthday,sex,telephone,email,status,code) VALUES(?,?,?,?,?,?,?,?,?)";
        try {
            jt.update(sql,
                    user.getUsername(),
                    user.getPassword(),
                    user.getName(),
                    user.getBirthday(),
                    user.getSex(),
                    user.getTelephone(),
                    user.getEmail(),
                    user.getStatus(),
                    user.getCode()
            );
            return true;
        } catch (Exception e) {
//            注册失败返回false
            return false;
        }
    }

    @Override
    public User findCode(String code) {
        String sql = "SELECT * FROM tab_user WHERE code = ?";
        try {
            User user = jt.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), code);
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void updateStatus(User user) {
        String sql = "UPDATE tab_user SET status=? WHERE code=?";
        jt.update(sql, "Y", user.getCode());
    }

    @Override
    public User findByUserNameAndPassword(String userName, String password) {
        String sql = "SELECT * FROM tab_user WHERE username=? and password=?";

        try {
            User user = jt.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), userName, password);
            return user;
        } catch (Exception e) {
            return null;
        }
    }
}
