package cn.itcast.travel.dao.impl;

import cn.itcast.travel.dao.RouteDao;
import cn.itcast.travel.domain.Favorite;
import cn.itcast.travel.domain.Route;
import cn.itcast.travel.util.JDBCUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author  Created by 丶Anton on 2021/3/12 13:23
 */
public class RouteDaoImpl implements RouteDao {
    private JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());

    @Override
    public int countAll(int cid, String rName) {
//        String sql = "SELECT COUNT(*) FROM tab_route WHERE cid=?";
        //1.定义sql模板
        String sql = "select count(*) from tab_route where 1=1 ";
        StringBuilder sb = new StringBuilder(sql);
        //条件们
        List list = new ArrayList();
        //2.判断参数是否有值
        if(cid > 0){
            sb.append( " and cid = ? ");
            //添加？对应的值
            list.add(cid);
        }

        if(rName != null){
            if (!"null".equals(rName) && rName.length() > 0) {
                sb.append(" and rname like ? ");
                list.add("%" + rName + "%");
            }
        }

        sql = sb.toString();
        Integer totalRecord = jt.queryForObject(sql, Integer.class, list.toArray());
        return totalRecord;
    }

    @Override
    public int countAll(int currentPrice, int endPrice, String rName) {
        String sql = "SELECT count(*) FROM tab_route WHERE 1=1 ";
        StringBuilder sb = new StringBuilder(sql);
        List list = new ArrayList();
        if(currentPrice > 0){
            sb.append(" AND price > ? ");
            list.add(currentPrice);
        }
        if(endPrice > 0){
            sb.append(" AND price < ? ");
            list.add(endPrice);
        }
        if(rName != null){
            if(!"null".equals(rName) && rName.length() > 0 && !"undefined".equals(rName)) {
                sb.append(" AND rname LIKE ? ");
                list.add("%" + rName + "%");
            }
        }
        sql = sb.toString();
        return jt.queryForObject(sql, Integer.class, list.toArray());
    }

    @Override
    public List<Route> getLimitPageBean(int cid, int startPage, int pageSize, String rName) {
        String sql = "SELECT * FROM tab_route WHERE 1=1 ";
        StringBuilder sb = new StringBuilder(sql);
        List list = new ArrayList();
        if(cid > 0){
            sb.append(" AND cid=? ");
            list.add(cid);
        }

        if(!"null".equals(rName) && !"".equals(rName)){
            sb.append(" AND rname LIKE ?");
            list.add("%" + rName + "%");
        }
        sb.append(" LIMIT ?, ?");
        list.add(startPage);
        list.add(pageSize);
        sql = sb.toString();
        try {
            List<Route> route = jt.query(sql, new BeanPropertyRowMapper<Route>(Route.class),list.toArray());
            return route;
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public Route find(int rid) {
        String sql = "SELECT * FROM tab_route WHERE rid = ?";
        return jt.queryForObject(sql, new BeanPropertyRowMapper<Route>(Route.class), rid);
    }

    @Override
    public List<Route> findRecommend() {
        String sql = "SELECT * FROM tab_route ORDER BY count DESC LIMIT 5";
        return jt.query(sql, new BeanPropertyRowMapper<Route>(Route.class));
    }

    @Override
    public List<Route> findAllKeep(List<Favorite> favorites, int currentPage) {
        String sql = "SELECT * FROM tab_route WHERE rid IN(0";
        StringBuilder sb = new StringBuilder(sql);
        List list = new ArrayList();
        for(int i=0; i < favorites.size(); i++){
            sb.append(",?");
            list.add(favorites.get(i).getRid());
        }
        sb.append(")  LIMIT ?, 12");
        list.add(currentPage);
        sql = sb.toString();
        List<Route> query = jt.query(sql, new BeanPropertyRowMapper<Route>(Route.class), list.toArray());
        return query;
    }

    @Override
    public List<Route> findFavoritesRanking(int currentPrice, int endPrice, int current, String rName) {
        String sql = "SELECT * FROM `tab_route` WHERE 1=1";
        StringBuilder sb = new StringBuilder(sql);
        List list = new ArrayList();
        if(currentPrice > 0){
            sb.append(" AND price > ? ");
            list.add(currentPrice);
        }

        if(endPrice > 0){
            sb.append(" AND price < ? ");
            list.add(endPrice);
        }

        if(rName != null){
            if (!"null".equals(rName) && rName.length() > 0 && !"undefined".equals(rName)) {
                sb.append(" AND rname LIKE ? ");
                list.add("%" + rName + "%");
            }
        }

        list.add(current);

        sb.append(" ORDER BY COUNT DESC LIMIT ?,10");
        sql = sb.toString();
        try {
            return jt.query(sql, new BeanPropertyRowMapper<Route>(Route.class), list.toArray());
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Route> findPopularity(int cid, String field, int pageSize) {
        String sql = "SELECT * FROM tab_route WHERE 1=1 ";
        List list = new ArrayList();
        StringBuilder sb = new StringBuilder(sql);
        if (cid > 0) {
            sb.append(" AND cid = ? ");
            list.add(cid);
        }

        if("count".equalsIgnoreCase(field)){
            sb.append(" ORDER BY COUNT DESC ");
        }else{
            sb.append(" ORDER BY rdate DESC ");
        }
        sb.append(" LIMIT ? ");
        list.add(pageSize);
        sql = sb.toString();
        try {
            return jt.query(sql, new BeanPropertyRowMapper<Route>(Route.class), list.toArray());
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Route> findTheme(String rName) {
        String sql = "SELECT * FROM tab_route WHERE rName LIKE ? ORDER BY count DESC LIMIT 4";
        rName = "%" + rName +"%";
        try {
            return jt.query(sql, new BeanPropertyRowMapper<Route>(Route.class), rName);
        } catch (DataAccessException e) {
            return null;
        }
    }


}
