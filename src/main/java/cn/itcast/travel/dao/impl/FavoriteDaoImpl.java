package cn.itcast.travel.dao.impl;

import cn.itcast.travel.dao.FavoriteDao;
import cn.itcast.travel.domain.Favorite;
import cn.itcast.travel.util.JDBCUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Created by 丶Anton on 2021/3/15 9:01
 */
public class FavoriteDaoImpl implements FavoriteDao {
    private JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());


    @Override
    public List<Favorite> findKeepRoute(int rid, int uid) {
        String sql = "SELECT * FROM tab_favorite WHERE 1=1 ";
        StringBuilder sb = new StringBuilder(sql);
        List list = new ArrayList();
        /*判断拼接SQL   如果rid大于0则进行添加*/
        if(rid > 0){
            sb.append("  AND rid = ? ");
            list.add(rid);
        }
        /*判断拼接SQL   如果uid大于0则进行添加*/
        if(uid > 0){
            sb.append("  AND uid = ? ");
            list.add(uid);
        }
        /*将拼接好的SQL赋值给原SQL*/
        sql = sb.toString();
        try {
            /*查询*/
            return jt.query(sql, new BeanPropertyRowMapper<Favorite>(Favorite.class), list.toArray());
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public void addKeep(int rid, int uid) {
        String sql = "INSERT INTO tab_favorite VALUE(?, ?, ?)";
        Date currentTime = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        //将日期对象格式化成指定格式并以String输出
        String date = simpleDateFormat.format(currentTime);

        jt.update(sql, rid, date, uid);
    }

    @Override
    public void remove(int rid, int uid) {
        String sql = "DELETE FROM tab_favorite WHERE rid=? AND uid=?";
        jt.update(sql, rid, uid);
    }

    @Override
    public void addDelete(int rid, int count, boolean b) {
        String sql = "UPDATE tab_route SET count=? WHERE rid=?";
        /*真加添加   假为--*/
        if(b){
            count += 1;
        }else{
            count -= 1;
        }
        jt.update(sql, count, rid);
    }
}
