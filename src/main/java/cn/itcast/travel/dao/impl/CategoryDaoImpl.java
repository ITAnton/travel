package cn.itcast.travel.dao.impl;

import cn.itcast.travel.dao.CategoryDao;
import cn.itcast.travel.domain.Category;
import cn.itcast.travel.util.JDBCUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * @author  Created by 丶Anton on 2021/3/11 17:03
 */
public class CategoryDaoImpl implements CategoryDao {
    private JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());

    @Override
    public List<Category> findAllCategoryDao() {
        String sql = "SELECT cid,cname FROM tab_category";
        List<Category> category = null;
        try {
            category = jt.query(sql, new BeanPropertyRowMapper<Category>(Category.class));
        } catch (DataAccessException e) {
            return null;
        }
        return category;
    }

    @Override
    public Category findOne(int cid) {
        String sql = "SELECT * FROM tab_category WHERE cid = ?";
        try {
            return jt.queryForObject(sql, new BeanPropertyRowMapper<Category>(Category.class), cid);
        } catch (DataAccessException e) {
            return null;
        }

    }
}
