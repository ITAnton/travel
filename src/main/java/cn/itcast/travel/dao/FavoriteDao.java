package cn.itcast.travel.dao;

import cn.itcast.travel.domain.Favorite;

import java.util.List;

/**
 * @author Created by 丶Anton on 2021/3/15 9:01
 */
public interface FavoriteDao {

    /**
     * 通过路线rid 与 用户uid查询匹配的收藏记录
     * @param rid
     * @param uid
     * @return
     */
    List<Favorite> findKeepRoute(int rid, int uid);

    /**
     * 收藏切换
     * @param rid
     * @param uid
     */
    void addKeep(int rid, int uid);

    /**
     * 取消收藏  删除该条记录
     * @param rid
     * @param uid
     */
    void remove(int rid, int uid);

    /**
     * 收藏次数添加与减
     * @param rid
     *  @param count
     * @param b
     */
    void addDelete(int rid, int count, boolean b);


}
