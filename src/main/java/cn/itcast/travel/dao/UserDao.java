package cn.itcast.travel.dao;

import cn.itcast.travel.domain.User;

/**
 * Created by 丶Anton on 2021/3/10 10:08
 */
public interface UserDao {

    /**
     * 查询用户名
     * @param userName 用户填写的用户名
     * @return
     */
    String findUserName(String userName);

    /**
     * 注册用户
     * @param user 前台封装的用户信息
     * @return
     */
    boolean registered(User user);

    /**
     * 用户激活码查询
     * @param code
     * @return
     */
    User findCode(String code);

    /**
     * 激活用户
     * @param user
     */
    void updateStatus(User user);

    User findByUserNameAndPassword(String userName, String password);
}
