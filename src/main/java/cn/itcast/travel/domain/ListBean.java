package cn.itcast.travel.domain;

import java.util.List;

/**
 * @author Created by 丶Anton on 2021/3/15 22:38
 */
public class ListBean<T> {

    /*人气*/
    private List<T> popularity;
    /*主题*/
    private List<T> theme;
    /*最新*/
    private List<T> latest;
    /*国内*/
    private List<T> domestic;

    public List<T> getPopularity() {
        return popularity;
    }

    public void setPopularity(List<T> popularity) {
        this.popularity = popularity;
    }

    public List<T> getTheme() {
        return theme;
    }

    public void setTheme(List<T> theme) {
        this.theme = theme;
    }

    public List<T> getLatest() {
        return latest;
    }

    public void setLatest(List<T> latest) {
        this.latest = latest;
    }

    public List<T> getDomestic() {
        return domestic;
    }

    public void setDomestic(List<T> domestic) {
        this.domestic = domestic;
    }
}
