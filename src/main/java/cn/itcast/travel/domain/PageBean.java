package cn.itcast.travel.domain;

import java.util.List;

/**
 * @author Created by 丶Anton on 2021/3/12 13:44
 */
public class PageBean<T> {

    /**
     * @totalCount 总记录数
     * @totalPages 总页数
     * @startPage 开始位置
     * @pageSize 显示条数
     * @list 每页展示的数据List集合
     */
    private int totalCount;
    private int totalPages;
    private int startPage;
    private int pageSize;

    private List<T> list;

    @Override
    public String toString() {
        return "PageBean{" +
                "totalCount=" + totalCount +
                ", totalPages=" + totalPages +
                ", startPage=" + startPage +
                ", pageSize=" + pageSize +
                ", list=" + list +
                '}';
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
