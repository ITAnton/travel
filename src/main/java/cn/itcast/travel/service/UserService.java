package cn.itcast.travel.service;

import cn.itcast.travel.domain.User;

/**
 * Created by 丶Anton on 2021/3/10 10:09
 */
public interface UserService {
    /**
     * 注册用户
     * @param user
     * @return
     */
    boolean registered(User user);

    /**
     * 激活用户
     * @param code
     * @return
     */
    boolean activation(String code);

    /**
     * 登录
     * @param user
     * @return
     */
    User login(User user);

    /**
     * 判断用户名是否存在
     * @param userName
     * @return
     */
    boolean nameJudge(String userName);
}
