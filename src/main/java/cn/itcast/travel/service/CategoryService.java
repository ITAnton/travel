package cn.itcast.travel.service;

import cn.itcast.travel.domain.Category;

import java.util.List;

/**
 * Created by 丶Anton on 2021/3/11 17:07
 */
public interface CategoryService {

    List<Category> findAllCategory();

}
