package cn.itcast.travel.service.impl;

import cn.itcast.travel.dao.*;
import cn.itcast.travel.dao.impl.*;
import cn.itcast.travel.domain.*;
import cn.itcast.travel.service.RouteService;
import cn.itcast.travel.util.JedisUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * @author Created by 丶Anton on 2021/3/12 13:57
 */
public class RouteServiceImpl implements RouteService {
    private RouteDao dao = new RouteDaoImpl();
    private RouteImgDao imgDao = new RouteImgDaoImpl();
    private SellerDao sDao = new SellerDaoImpl();
    private CategoryDao cDao = new CategoryDaoImpl();
    private FavoriteDao fDao = new FavoriteDaoImpl();


    @Override
    public PageBean<Route> pageQuery(int cid, int currentPage, int pageSize, String rName) {
        /*创建PageBean对象*/
        PageBean<Route> pb = new PageBean<Route>();
        /*封装PageBean*/
        /*1.1 查询该cid全部记录条数*/
        int countAll = 0;
        countAll = dao.countAll(cid, rName);
        pb.setTotalCount(countAll);
        /*1.2 计算总页数 总条数/显示条数 整除等于结果 否则+1*/
        int totalPages = countAll % pageSize == 0 ? countAll / pageSize : (countAll / pageSize) + 1;
        pb.setTotalPages(totalPages);
        /*1.3 设置开始位置 当前页码 - 1 * 每页显示条数*/
        int startPage = (currentPage - 1)  * pageSize;
        pb.setStartPage(startPage);
        /*1.4 设置显示条数 */
        pb.setPageSize(pageSize);
        /*1.5 封装本页显示的数据 开始位置 */
        List<Route> list = dao.getLimitPageBean(cid, startPage, pageSize, rName);
        pb.setList(list);
        return pb;
    }

    @Override
    public Route find(String rid) {
        /*通过rid查询该商品基本i*/
        Route route = dao.find(Integer.parseInt(rid));
        /*通过rid查询出该商品的所有大小图片信息*/
        List<RouteImg> imgId = imgDao.findImgId(route.getRid());
        /*通过sid查询该商品的商家信息*/
        Seller all = sDao.findAll(route.getSid());
        /*查询该商品所属分类*/
        Category one = cDao.findOne(route.getCid());
        /*将该商品所有大小图片放入 游线路商品实体类 中*/
        route.setRouteImgList(imgId);
        /*将商家基本信息放入 游线路商品实体类 中*/
        route.setSeller(all);
        /*将商品分类放入 商品实体类中*/
        route.setCategory(one);
        /*返回该实体类*/
        return route;
    }

    @Override
    public List<Route> findRecommend(){
        return dao.findRecommend();
    }

    @Override
    public PageBean<Route> userFavorites(int uid, String currentPageStr) {
        PageBean<Route> pageBean = new PageBean<Route>();
        /*获取用户所有收藏路线*/
        List<Favorite> keepRoute = fDao.findKeepRoute(0, uid);
        /*设置总记录条数 */
        int totalCount = keepRoute.size();
        /*存放总记录条数*/
        pageBean.setTotalCount(totalCount);
        /*计算总页码 总页码=总记录数/显示条数(12)*/
        int totalPages = totalCount % 12 == 0 ? totalCount / 12 : (totalCount / 12) + 1;
        /*存放总记录条数*/
        pageBean.setTotalPages(totalPages);
        /*计算开始位置 (当前页码 - 1) * 12*/
        int currentPage = 0;
        if(currentPageStr != null && currentPageStr.length() > 0 && !"null".equals(currentPageStr)){
            currentPage = (Integer.parseInt(currentPageStr) - 1) * 12;
        }
        /*存放当前开始位置*/
        pageBean.setStartPage(currentPage);
        /*存放显示条数*/
        pageBean.setPageSize(12);
        /*通过数据库搜索本页所有记录*/
        List<Route> allKeep = dao.findAllKeep(keepRoute, currentPage);
        /*存放本页路线数据*/
        pageBean.setList(allKeep);
        return pageBean;
    }

    @Override
    public PageBean<Route> findFavoritesRanking(String currentPrice, String endPrice, String current, String rName){
        PageBean<Route> pageBean = new PageBean<Route>();
        /*查询总记录条数*/
        int startPage = 0;
        /*判断传递的current是否为空*/
        if(current != null){
            if(current.length() > 0 && !"null".equals(current) && !"undefined".equals(current)){
                /*计算开始位置*/
                startPage = (Integer.parseInt(current) - 1) * 10;
            }
        }
        int maxPrice = 0;
        if(endPrice != null){
            if(endPrice.length() > 0 && !"null".equals(endPrice) && !"undefined".equals(endPrice)){
                /*计算开始位置*/
                maxPrice = Integer.parseInt(endPrice);
            }
        }
        int minPrice = 0;
        if(currentPrice != null){
            if(currentPrice.length() > 0 && !"null".equals(currentPrice) && !"undefined".equals(currentPrice)){
                /*计算开始位置*/
                minPrice = Integer.parseInt(currentPrice);
            }
        }
        int totalCount = dao.countAll(minPrice, maxPrice, rName);
        /*存放记录条数*/
        pageBean.setTotalCount(totalCount);
        /*计算总页数 */
        int totalPages = totalCount % 10 == 0 ? totalCount / 10 : (totalCount / 10) + 1;
        /*存放总页数*/
        pageBean.setTotalPages(totalPages);
        /*存放每页显示条数*/
        pageBean.setPageSize(10);
        /*存放开始位置*/
        pageBean.setStartPage(startPage);
        /*设置最小money与最大money*/
        int minMoney = 0;
        if(currentPrice != null && currentPrice.length() > 0 && !"null".equals(currentPrice) && !"undefined".equals(currentPrice)){
            minMoney = Integer.parseInt(currentPrice);
        }
        int maxMoney = 0;
        if(endPrice != null && endPrice.length() > 0 && !"null".equals(endPrice) && !"undefined".equals(endPrice)){
            maxMoney = Integer.parseInt(endPrice);
        }

        /*获取本页数据*/
        List<Route> favoritesRanking = dao.findFavoritesRanking(minMoney, maxMoney, startPage, rName);
        /*存放本页显示数据*/
        pageBean.setList(favoritesRanking);
        return pageBean;
    }

    @Override
    public String findPopularity() {
        Jedis jedis = JedisUtil.getJedis();
        /*判断redis中是否有数据*/
        String indexRoute = jedis.get("indexRoute");

        /*数据库中存有数据*/
        if(indexRoute == null || indexRoute.length() == 0 || "".equals(indexRoute)){
            ListBean<Route> listBean = new ListBean<Route>();
            /*查询人气路线*/
            List<Route> popularity = dao.findPopularity(0, "count", 4);
            /*存放人气路线*/
            listBean.setPopularity(popularity);
            /*查询最新路线*/
            List<Route> latestRoute = dao.findPopularity(0, "rdate", 4);
            /*存放最新路线*/
            listBean.setLatest(latestRoute);
            /*查询主题路线*/
            List<Route> theme = dao.findTheme("海南");
            /*存放主题路线*/
            listBean.setTheme(theme);
            /*查询国内人气路线*/
            List<Route> domestic = dao.findPopularity(5, "count", 6);
            /*存放国内人气路线*/
            listBean.setDomestic(domestic);

            ObjectMapper mapper = new ObjectMapper();
            try {
                /*将ListBean对象转换为JSON格式*/
                indexRoute = mapper.writeValueAsString(listBean);
                /*将数据放入redis数据库*/
                jedis.set("indexRoute", indexRoute);
            } catch (JsonProcessingException e) {
                return null;
            }
        }
        return indexRoute;
    }


}
