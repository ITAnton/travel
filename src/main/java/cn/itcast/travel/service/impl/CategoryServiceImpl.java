package cn.itcast.travel.service.impl;

import cn.itcast.travel.dao.CategoryDao;
import cn.itcast.travel.dao.impl.CategoryDaoImpl;
import cn.itcast.travel.domain.Category;
import cn.itcast.travel.service.CategoryService;
import cn.itcast.travel.util.JedisUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Created by 丶Anton on 2021/3/11 17:07
 */
public class CategoryServiceImpl implements CategoryService {
    private CategoryDao dao = new CategoryDaoImpl();

    @Override
    public List<Category> findAllCategory() {
//        List<Category> category = dao.findAllCategoryDao();
        /*获取Jedis对象*/
        Jedis jedis = JedisUtil.getJedis();
        /*从redis数据库中查找key*/
        Set<Tuple> category = jedis.zrangeWithScores("category", 0, -1);
        List<Category> lis = null;
        /*判断是否为第一次加载页面*/
        if(category == null || category.size() == 0){
            /*第一次加载页面则存放标签名进入redis数据库*/
            /*从MySQL数据库中获取所有标签名*/
            lis = dao.findAllCategoryDao();
            /*将所有标签名放入redis数据库*/
            for (Category li : lis) {
                jedis.zadd("category", li.getCid(), li.getCname());
            }
            /*再一次从redis中取出数据  保持顺序正确*/
            category = jedis.zrangeWithScores("category", 0, -1);
        }
        /*不是第一次加载页面则将category中的数据放入list集合*/
        lis = new ArrayList<Category>();
        for (Tuple tuple : category) {
            Category c = new Category();
            c.setCname(tuple.getElement());
            c.setCid((int)tuple.getScore());
            lis.add(c);
         }
        return lis;
    }
}
