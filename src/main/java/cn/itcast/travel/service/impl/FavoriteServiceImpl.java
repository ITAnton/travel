package cn.itcast.travel.service.impl;

import cn.itcast.travel.dao.FavoriteDao;
import cn.itcast.travel.dao.RouteDao;
import cn.itcast.travel.dao.impl.FavoriteDaoImpl;
import cn.itcast.travel.dao.impl.RouteDaoImpl;
import cn.itcast.travel.domain.Favorite;
import cn.itcast.travel.domain.Route;
import cn.itcast.travel.service.FavoriteService;

import java.util.List;

/**
 * @author Created by 丶Anton on 2021/3/15 9:12
 */
public class FavoriteServiceImpl implements FavoriteService {
    private FavoriteDao dao = new FavoriteDaoImpl();

    /**
     * 用户收藏路线查询
     * @param ridStr
     * @param uidStr
     * @return
     */
    @Override
    public boolean findKeep(String ridStr, String uidStr) {
        int rid = 0;
        List<Favorite> keepRoute = null;
        if(ridStr != null && !"null".equals(ridStr) && ridStr.length() > 0){
            rid = Integer.parseInt(ridStr);
            int uid = 0;
            if(uidStr != null && !"null".equals(uidStr) && uidStr.length() > 0){
                uid = Integer.parseInt(uidStr);
                keepRoute = dao.findKeepRoute(rid, uid);
                if(keepRoute.size() > 0 && keepRoute != null){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String favoritesSwitch(String ridStr, String uidStr) {
        RouteDao rDao = new RouteDaoImpl();
        if(uidStr == null || uidStr.length() == 0 || "null".equals(uidStr)){
            return null;
        }
        int rid = Integer.parseInt(ridStr);
        Route route = rDao.find(rid);
        int uid = Integer.parseInt(uidStr);
        /*查询当前用户是否收藏过该商品*/
        List<Favorite> keepRoute = dao.findKeepRoute(rid, uid);
        /*判断用户是否收藏过此商品*/
        if(keepRoute != null && keepRoute.size() > 0){
            /*route表中count字段-1*/
            /*收藏过商品则  删除该条记录*/
            dao.addDelete(rid, route.getCount(), false);
            dao.remove(rid, uid);
            return "取消收藏成功";
        }
        /*未收藏  添加收藏记录*/
        /*route表中count字段+1*/
        dao.addDelete(rid, route.getCount(), true);
        dao.addKeep(rid, uid);
        return "收藏成功";
    }
}
