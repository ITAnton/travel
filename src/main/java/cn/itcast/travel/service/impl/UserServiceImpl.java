package cn.itcast.travel.service.impl;

import cn.itcast.travel.dao.UserDao;
import cn.itcast.travel.dao.impl.UserDaoImpl;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.service.UserService;
import cn.itcast.travel.util.MailUtils;
import cn.itcast.travel.util.UuidUtil;

/**
 * Created by 丶Anton on 2021/3/10 10:09
 */
public class UserServiceImpl implements UserService {

    private UserDao dao = new UserDaoImpl();


    @Override
    public boolean registered(User user) {
        try {
//            生成激活码
            user.setCode(UuidUtil.getUuid());
//            存放激活状态
            user.setStatus("N");
//            开始注册
            dao.registered(user);
//            设置超链接标签 --> 发送激活邮件
            String a = "<a href=\"http://www.itanton.site/user/activation?code=" + user.getCode() + "\">点击激活【Anton旅游网】</a>";
            MailUtils.sendMail(user.getEmail(),a,"Anton旅游网");
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean activation(String code) {
        /*通过code查询指定用户信息*/
        try {
            User user = dao.findCode(code);
            if(user != null){
                /*开始激活*/
                dao.updateStatus(user);
                return true;
            }
            return false;
        } catch (Exception e) {
            /*查询/激活失败*/
            return false;
        }
    }

    @Override
    public User login(User user) {
        User serverUser = dao.findByUserNameAndPassword(user.getUsername(), user.getPassword());
        if(serverUser != null){
            return serverUser;
        }
        return null;
    }

    @Override
    public boolean nameJudge(String userName) {
        if(userName != null && !"".equals(userName) && userName.length() > 0){
            String userName1 = dao.findUserName(userName);
            if(userName1 == null){
                return false;
            }
        }
        return true;
    }



}
