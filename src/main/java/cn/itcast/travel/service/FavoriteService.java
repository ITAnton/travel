package cn.itcast.travel.service;

/**
 * @author Created by 丶Anton on 2021/3/15 9:12
 */
public interface FavoriteService {

    /**
     * 查询用户是否收藏指定路线
     * @param ridStr
     * @param uidStr
     * @return
     */
    boolean findKeep(String ridStr, String uidStr);

    /**
     * 收藏切换
     * @param rid
     * @param uid
     * @return
     */
    String favoritesSwitch(String rid, String uid);

}
