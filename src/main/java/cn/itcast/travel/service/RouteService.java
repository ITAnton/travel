package cn.itcast.travel.service;

import cn.itcast.travel.domain.ListBean;
import cn.itcast.travel.domain.PageBean;
import cn.itcast.travel.domain.Route;

import java.util.List;

/**
 * @author Created by 丶Anton on 2021/3/12 13:56
 */
public interface RouteService {
    /**
     * 分页查询
     * @param cid
     * @param currentPage
     * @param pageSize
     * @param rName
     * @return
     */
    PageBean<Route> pageQuery(int cid, int currentPage, int pageSize, String rName);

    /**
     * 查询指定商品数据
     * @param rid
     * @return
     */
    Route find(String rid);

    /**
     * 查询最热门的5条路线
     * @return
     */
    List<Route> findRecommend();

    /**
     * 查询指定用户所有收藏路线
     * @param uid
     * @param currentPageStr
     * @return
     */
    PageBean<Route> userFavorites(int uid, String currentPageStr);

    /**
     * 收藏排行榜Service
     * @param currentPrice
     * @param endPrice
     * @param current
     * @param rName
     * @return
     */
    PageBean<Route> findFavoritesRanking(String currentPrice, String endPrice, String current, String rName);

    /**
     * 人气查询   转为JSON格式存放入redis数据库
     * @return
     */
    String findPopularity();

}
