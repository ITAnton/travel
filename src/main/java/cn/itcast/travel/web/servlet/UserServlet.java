package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.ResultInfo;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.service.UserService;
import cn.itcast.travel.service.impl.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Created by 丶Anton on 2021/3/11 15:36
 */
@WebServlet("/user/*")
public class UserServlet extends BaseServlet {
    private UserService service = new UserServiceImpl();

    private String getJson(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }

    /**
     * 用户登录
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ResultInfo info = new ResultInfo();
        //        获取验证码
        String check = request.getParameter("check");
//        获取server验证码
        HttpSession session = request.getSession();
        String checkCode_server = (String)session.getAttribute("checkCode_server");
//        删除
        session.removeAttribute("checkCode_server");
//        判断
        if(check == null || !check.equalsIgnoreCase(checkCode_server)){
//            验证码错误
            info.setErrorMsg("验证码错误！");
            info.setFlag(false);
        }else{
//            封装数据
            User user = new User();
            try {
                BeanUtils.populate(user, request.getParameterMap());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            /*验证用户信息*/
            User serverUser = service.login(user);
            /*验证状态*/
            if(serverUser == null){
                info.setErrorMsg("用户名或密码错误！");
                info.setFlag(false);
            }else{
                /*验证激活状态*/
                String status = serverUser.getStatus();
                if(!status.equals("Y")){
                    /*用户未激活*/
                    info.setErrorMsg("您的账户未进行激活操作~");
                    info.setFlag(false);
                }else{
//                    将登录后的用户信息存入server
                    request.getSession().setAttribute("user",serverUser);
                    info.setFlag(true);
                }
            }
        }
        String data = getJson(info);
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(data);
    }

    /**
     * 登录的用户信息
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void find(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User)request.getSession().getAttribute("user");
        String json = getJson(user);
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(json);
    }

    /**
     * 退出
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void out(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*销毁Session*/
        request.getSession().invalidate();
        response.sendRedirect( request.getContextPath() + "/index.html");
    }

    /**
     * 用户激活
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void activation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String a;
//        获取激活码
        String code = request.getParameter("code");
//        比对激活码并进行判断
        boolean b = service.activation(code);
        if(b){
            /*激活成功*/
            a = "<a href=\"http://www.itanton.site/login.html\">恭喜您激活成功，请登录！</a>";
        }else{
            /*激活失败*/
            a = "激活失败,请联系管理员";
        }
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(a);
    }

    /**
     * 判断用户名是否存在
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void userNameJudge(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        boolean b = service.nameJudge(username);
        String json = getJson(b);
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(json);
    }


    /**
     * 用户注册
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //        封装注册结果对象
        ResultInfo info = new ResultInfo();
        HttpSession session = request.getSession();
        //      获取服务器验证码
        String checkCode_server = (String)session.getAttribute("checkCode_server");
        //      删除服务器验证码
        session.removeAttribute("checkCode_server");
        //        比较
        if(checkCode_server == null || !checkCode_server.equalsIgnoreCase(request.getParameter("check"))){
            //            验证码错误
            info.setFlag(false);
            //            错误信息
            info.setErrorMsg("验证码错误!");
        }else{
            //            验证码正确
            //        获取数据
            Map<String, String[]> map = request.getParameterMap();
            //        封装数据
            User user = new User();
            try {
                BeanUtils.populate(user, map);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            boolean flag = service.registered(user);
//        注册结果
            if(flag){
//            注册成功
                info.setFlag(true);
            }else{
//            注册失败
                info.setFlag(false);
//            错误信息
                info.setErrorMsg("注册失败！");
            }
        }
        response.setContentType("application/json;charset=utf-8");
//        将数据转换为JSON格式
        String data = getJson(info);
        //        数据回调
        response.getWriter().write(data);
    }


}
