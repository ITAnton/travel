package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.ListBean;
import cn.itcast.travel.domain.PageBean;
import cn.itcast.travel.domain.Route;
import cn.itcast.travel.domain.User;
import cn.itcast.travel.service.RouteService;
import cn.itcast.travel.service.impl.RouteServiceImpl;
import cn.itcast.travel.util.JedisUtil;
import cn.itcast.travel.util.JsonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * @author Created by 丶Anton on 2021/3/12 13:57
 */
@WebServlet("/route/*")
public class RouteServlet extends BaseServlet {
    RouteService service = new RouteServiceImpl();

    private ObjectMapper mapper = new ObjectMapper();

    private String ValueAsString(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    public void writeValue(Object object, HttpServletResponse response) throws Exception {
        response.setContentType("application/json;charset=utf-8");
        mapper.writeValue(response.getOutputStream(), object);
    }

    /**
     * 查询每页路线信息
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void pageQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*获取数据*/
        /*分类id*/
        String cidStr = request.getParameter("cid");
        /*当前页码*/
        /*无法接收当前页码*/
        String currentPageStr = request.getParameter("currentPage");
        /*每页显示条数*/
        String pageSizeStr = request.getParameter("pageSize");
        /*获取搜索路线*/
        String rName = request.getParameter("rname");
        /*如果不传递cid则默认为1*/
        int cid = 0;
        if (cidStr != null && cidStr.length() > 0 && !"null".equals(cidStr)) {
            cid = Integer.parseInt(cidStr);
        } else {
            cid = 5;
        }

        int currentPage = 0;
        if (currentPageStr != null && currentPageStr.length() > 0 && !"null".equals(currentPageStr)) {
            currentPage = Integer.parseInt(currentPageStr);
        } else {
            currentPage = 1;
        }


        int pageSize = 0;
        if (pageSizeStr != null && pageSizeStr.length() > 0 && !"null".equals(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        } else {
            pageSize = 5;
        }

        /*调用Service查询*/
        PageBean<Route> pb = service.pageQuery(cid, currentPage, pageSize, rName);

        try {
            /*将PageBean传递到客户端*/
            writeValue(pb, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据路线id查询一个路线的详情信息
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void routeDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*获取用户点击的具体商品id*/
        String rid = request.getParameter("rid");
        /*通过数据库搜索该商品信息*/
        Route route = service.find(rid);
        try {
            writeValue(route, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void findRecommend(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Route> recommend = service.findRecommend();
        try {
            writeValue(recommend, response);
        } catch (Exception e) {
        }
    }

    public void findKeep(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*获取用户id*/
        User user = (User) request.getSession().getAttribute("user");
        /*获取页码*/
        String currentPage = request.getParameter("currentPage");
        PageBean<Route> pageBean = service.userFavorites(user.getUid(), currentPage);
        try {
            writeValue(pageBean, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void findFavoritesRanking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        /*获取当前页码*/
        String currentPage = request.getParameter("currentPage");
        /*获取最小价格*/
        String minPrice = request.getParameter("minPrice");
        /*获取最大价格*/
        String manPrice = request.getParameter("manPrice");
        /*获取商品名*/
        String rname = request.getParameter("rname");


        PageBean<Route> favoritesRanking = service.findFavoritesRanking(minPrice, manPrice, currentPage, rname);
        try {
            writeValue(favoritesRanking, response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void findPopularity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        /*查询*/
        String json = service.findPopularity();
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(json);
    }
}
