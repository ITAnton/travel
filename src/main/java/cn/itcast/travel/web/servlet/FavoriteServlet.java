package cn.itcast.travel.web.servlet;

import cn.itcast.travel.domain.User;
import cn.itcast.travel.service.FavoriteService;
import cn.itcast.travel.service.impl.FavoriteServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Created by 丶Anton on 2021/3/15 9:19
 */
@WebServlet("/favorite/*")
public class FavoriteServlet extends BaseServlet {
    private FavoriteService service = new FavoriteServiceImpl();


    protected void writeValue(Object obj, HttpServletResponse response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.setContentType("application/json;charset=utf-8");
        mapper.writeValue(response.getOutputStream(), obj);
    }

    public void findAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User)request.getSession().getAttribute("user");
        /*获取参数*/
        String rid = request.getParameter("rid");
        /*调用Service查找*/
        boolean keep = false;
        if(user != null){
            keep = service.findKeep(rid, String.valueOf(user.getUid()));
        }else{
            keep = service.findKeep(rid, null);
        }
        writeValue(keep, response);
    }


    public void favoritesSwitch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User)request.getSession().getAttribute("user");
        /*获取参数*/
        String rid = request.getParameter("rid");

        String s = service.favoritesSwitch(rid, String.valueOf(user.getUid()));

        writeValue(s, response);
    }
}
