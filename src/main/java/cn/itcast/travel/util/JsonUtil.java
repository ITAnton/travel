package cn.itcast.travel.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Created by 丶Anton on 2021/3/12 14:55
 * 生成Json工具
 */
public class JsonUtil {

    private static ObjectMapper mapper = new ObjectMapper();

    private JsonUtil() {
    }

    /**
     * 将对象生成为Json数据
     * @param object
     * @return
     * @throws JsonProcessingException
     */
    public String ValueAsString(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    public static void writeValue(Object object, HttpServletResponse response) throws Exception {
        response.setContentType("application/json;charset=utf-8");
        mapper.writeValue(response.getOutputStream(), object);
    }


}
